def gcd(x, y):
    if x % y == 0:
        return y
    return gcd(y, x % y)


assert gcd(219, 93) == 3
