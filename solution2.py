def population(n):
    if n == 0:
        return 1000000
    return 1.01 * population(n - 1) + 1000


assert population(0) == 1000000
assert population(1) == 1011000
