def steps(x):
    if x == 1:
        return 1
    elif x == 2:
        return 2
    elif x == 3:
        return 4
    else:
        return steps(x - 1) + steps(x - 2) + steps(x - 3)


assert steps(1) == 1
assert steps(2) == 2
assert steps(3) == 4
assert steps(6) == 24
