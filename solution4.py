def is_prime(n, i=2):
    if n <= 2:
        return True if n == 2 else False
    if n % i == 0:
        return False
    if i * i > n:
        return True

    return is_prime(n, i + 2)


assert is_prime(11)
assert not is_prime(10)
